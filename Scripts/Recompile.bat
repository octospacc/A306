@ECHO OFF

ECHO [ Recompiling Code ]
RD /S /Q Output > NUL 2> NUL
apktool b Source -o Output\A306.apk

ECHO : reverting bspatches
MOVE /Y Source\apktool.yml.bak Source\apktool.yml > NUL 2> NUL
MOVE /Y Source\AndroidManifest.xml.bak Source\AndroidManifest.xml > NUL 2> NUL
MOVE /Y Source\smali_classes4\com\linecorp\b612\android\view\dialog\InvalidUseDialogFragment.smali.bak Source\smali_classes4\com\linecorp\b612\android\view\dialog\InvalidUseDialogFragment.smali > NUL 2> NUL