@ECHO OFF

ECHO [ Patching Code ]

ECHO : manifests patches
COPY /Y Source\apktool.yml Source\apktool.yml.bak > NUL 2> NUL
bspatch Source\apktool.yml.bak Source\apktool.yml Patches\Adaptive\apktool.yml.bspatch
COPY /Y Source\AndroidManifest.xml Source\AndroidManifest.xml.bak > NUL 2> NUL
bspatch Source\AndroidManifest.xml.bak Source\AndroidManifest.xml Patches\Adaptive\AndroidManifest.xml.bspatch

ECHO : apksign check inhibition
COPY /Y Source\smali_classes4\com\linecorp\b612\android\view\dialog\InvalidUseDialogFragment.smali Source\smali_classes4\com\linecorp\b612\android\view\dialog\InvalidUseDialogFragment.smali.bak > NUL 2> NUL
bspatch Source\smali_classes4\com\linecorp\b612\android\view\dialog\InvalidUseDialogFragment.smali Source\smali_classes4\com\linecorp\b612\android\view\dialog\InvalidUseDialogFragment.smali Patches\Adaptive\InvalidUseDialogFragment.smali.bspatch
REM COPY /Y Patches\InvalidUseDialogFragment.smali Source\smali_classes4\com\linecorp\b612\android\view\dialog\ > NUL 2> NUL
REM Support\FnR.exe --cl --silent --dir "Source\smali_classes4\com\linecorp\b612\android\view\dialog" --fileMask "InvalidUseDialogFragment.smali" --find "    if-eqz p1, :cond_0\n\n    const p2, 0x7f10003b\n\n    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V\n\n    return-void\n\n    :cond_0\n    const-string p1, ""messageTextView""\n\n    invoke-static {p1}, Lkotlin/jvm/internal/k;->r(Ljava/lang/String;)V\n\n    const/4 p1, 0x0\n\n    throw p1" --replace ""

ECHO : removing non-english strings (because of inconsistencies)
FOR /D %%G IN (Source\res\values-??) DO RD /S /Q "%%~G" > NUL 2> NUL
FOR /D %%G IN (Source\res\values-?+*) DO RD /S /Q "%%~G" > NUL 2> NUL
FOR /D %%G IN (Source\res\values-??-*) DO RD /S /Q "%%~G" > NUL 2> NUL

ECHO : removing useless/spyware files/folders
RD /S /Q Source\kotlin > NUL 2> NUL
RD /S /Q Source\unknown > NUL 2> NUL
RD /S /Q Source\META-INF > NUL 2> NUL
RD /S /Q Source\smali_assets > NUL 2> NUL
RD /S /Q Source\smali_classes4\com\mopub > NUL 2> NUL
RD /S /Q Source\smali_classes4\com\naver\gfpsdk\ext\mopub > NUL 2> NUL
RD /S /Q Source\smali_classes2\com\giphy\sdk\analytics > NUL 2> NUL
RD /S /Q Source\smali_classes2\com\giphy\sdk\tracking > NUL 2> NUL
RD /S /Q Source\smali_classes2\com\google\android\exoplayer2\analytics > NUL 2> NUL
RD /S /Q Source\smali_classes2\com\google\android\exoplayer2\drm > NUL 2> NUL
RD /S /Q Source\smali\com\android > NUL 2> NUL
RD /S /Q Source\smali_classes2\com\five_corp > NUL 2> NUL
RD /S /Q Source\smali_classes4\com\nhn > NUL 2> NUL
DEL /F /Q Source\assets\audience_network.dex > NUL 2> NUL
DEL /F /Q Source\assets\beauty_tutorial*.mp4 > NUL 2> NUL
DEL /F /Q Source\assets\mraid.js > NUL 2> NUL

REM : disabled (they make the app crash when removed)
REM RD /S /Q Source\smali\com\appsflyer > NUL 2> NUL
REM RD /S /Q Source\smali\com\facebook > NUL 2> NUL
REM RD /S /Q Source\smali_classes2\com\facebook > NUL 2> NUL
REM RD /S /Q Source\smali_classes2\com\google\android\gms > NUL 2> NUL
REM RD /S /Q Source\smali_classes3\com\google\firebase > NUL 2> NUL
REM RD /S /Q Source\smali_classes4\com\linecorp\b612\android\push\firebase > NUL 2> NUL
REM RD /S /Q Source\smali\com\facebook\ads > NUL 2> NUL
REM RD /S /Q Source\smali_classes2\com\google\android\gms\internal\ads > NUL 2> NUL
REM RD /S /Q Source\smali_classes4\com\yiruike > NUL 2> NUL
REM FOR /F "USEBACKQ" %%G IN (`DIR /B /S Source\*ads*`) DO RD /S /Q "%%~G" > NUL 2> NUL
REM FOR /F "USEBACKQ" %%G IN (`DIR /B /S Source\*firebase*`) DO RD /S /Q "%%~G" > NUL 2> NUL
REM FOR /F "USEBACKQ" %%G IN (`DIR /B /S Source\*analytics*`) DO RD /S /Q "%%~G" > NUL 2> NUL

REM ECHO : emptying unremovable useless files (errors here are normal)
REM FOR /F "USEBACKQ" %%G IN (`DIR /B /S Source\unknown`) DO NUL > "%%~G" 2> NUL

ECHO : finished patching!